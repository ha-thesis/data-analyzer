import { ILogger } from './ILogger.ts';


export class ConsoleLogger implements ILogger {
    log(log: string): void {
        console.log(`[${new Date().toLocaleString()}] ${log}`);
    }
}
