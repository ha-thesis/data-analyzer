import { ConsoleLogger } from "./loggers/ConsoleLogger.ts";
import { rabbitmqService } from "./services/rabbitmq.ts";
import { Analyzer } from "./services/analyzer.ts";
import { cockroachDBService } from "./services/cockroachDB.ts";
import { postgres } from "./deps.ts";

function getEnvVariable(variableName: string): string {
    const envVariable = Deno.env.get(variableName);
    if (envVariable === undefined) {
        throw new Error(`Environment variable ${variableName} is not set`);
    }
    return envVariable;
}

const consoleLogger: ConsoleLogger = new ConsoleLogger();
const rabbitMQUSername = getEnvVariable("RABBITMQ_USERNAME");
const rabbitMQPassword = getEnvVariable("RABBITMQ_PASSWORD");
const rabbitMQHost = getEnvVariable("RABBITMQ_HOST");
const rabbitQueueName = "data-ingress"
const rabbitMQ = new rabbitmqService(`amqp://${rabbitMQUSername}:${rabbitMQPassword}@${rabbitMQHost}`, rabbitQueueName);
await rabbitMQ.connect();
const rabbitMQChannel = rabbitMQ.getChannel();


const cockroachDBHost = getEnvVariable("COCKROACHDB_HOST");
const cockroachDBPort = Number(getEnvVariable("COCKROACHDB_PORT"));
const cockroachDBUsername = getEnvVariable("COCKROACHDB_USERNAME");
const cockroachDBPassword = getEnvVariable("COCKROACHDB_PASSWORD");
const cockroachDBDatabase = getEnvVariable("COCKROACHDB_DATABASE");
const cockroachdb = new cockroachDBService(cockroachDBHost, cockroachDBPort, cockroachDBUsername, cockroachDBPassword, cockroachDBDatabase, "ca.crt");
const cockraochDBClient: postgres.Client = await cockroachdb.connect() as postgres.Client;

const analyzer = new Analyzer(rabbitMQChannel, rabbitQueueName, cockraochDBClient);
await analyzer.consumeMessages();
