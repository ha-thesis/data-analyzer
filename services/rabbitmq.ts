// deno-lint-ignore-file no-explicit-any
import { rabbitmq } from "../deps.ts"
import { ConsoleLogger } from "../loggers/ConsoleLogger.ts";

export class rabbitmqService {
    private consoleLogger: ConsoleLogger = new ConsoleLogger();
    private connectionString: string;
    private queueString: string;
    private connection: any;
    private channel: any;

    constructor(url: string, queue: string) {
        this.connectionString = url;
        this.queueString = queue;
    }

    public async connect() {
        // limit to 1 message at a time
        try {
            this.connection = await rabbitmq.connect(this.connectionString);
            this.channel = await this.connection.openChannel();
            await this.channel.bindQueue({ queue: this.queueString, exchange: "amq.direct"});
            this.consoleLogger.log("Connected to RabbitMQ");
        } catch (error) {
            this.consoleLogger.log("Error connecting to RabbitMQ");
            this.consoleLogger.log(error.message)
            setTimeout(() => {
                Deno.exit(1);
            }, 1000);
        }

    }

    public async disconnect() {
        await this.channel.close();
        await this.connection.close();
    }

    public getChannel() {
        return this.channel;
    }

}