import { postgres } from "../deps.ts";
import { ConsoleLogger } from "../loggers/ConsoleLogger.ts";

export class cockroachDBService {
    private host: string;
    private port: number;
    private user: string;
    private password: string;
    private database: string;
    private caCrtFileLocation: string;
    private logger: ConsoleLogger;

    constructor(host: string, port: number, user: string, password: string, database: string, caCrtFileLocation: string) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database = database;
        this.caCrtFileLocation = caCrtFileLocation;
        this.logger = new ConsoleLogger();
    }

    private readCertificate(caCrtFileLocation: string): string {
        const decoder = new TextDecoder();
        const data = Deno.readFileSync(caCrtFileLocation);
        const certificate = decoder.decode(data);
        return certificate;
    }

    public async connect() {
        const certificate = this.readCertificate(this.caCrtFileLocation);
        const tlsOptions: postgres.TLSOptions = {
            enabled: true,
            enforce: true,
            caCertificates: [certificate],
        };
        try {
            const client = new postgres.Client({
                user: this.user,
                database: this.database,
                hostname: this.host,
                port: this.port,
                password: this.password,
                tls: tlsOptions
            });
            await client.connect();
            this.logger.log("Connected to CockroachDB");
            return client;
        } catch (error) {
            console.log(error.message);
            setTimeout(() => {
                Deno.exit(1);
            }, 1000);
        }
    }

}