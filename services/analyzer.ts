import { ConsoleLogger } from './../loggers/ConsoleLogger.ts';
import { DeviceData } from './../models/DeviceData.ts';
import { postgres } from '../deps.ts';
// deno-lint-ignore-file no-explicit-any

export class Analyzer {
    private rabbitMQChannel: any;
    private queueName: string;
    private cockraochDBClient: postgres.Client;
    private logger: ConsoleLogger;

    constructor(rabbitMQChannel: any, queueName: string, cockraochDBClient: postgres.Client) {
        this.rabbitMQChannel = rabbitMQChannel;
        this.queueName = queueName;
        this.cockraochDBClient = cockraochDBClient;
        this.logger = new ConsoleLogger();
    }

    public async consumeMessages() {
        await this.rabbitMQChannel.qos({ prefetchCount: 5 });
        await this.rabbitMQChannel.consume(
            { queue: this.queueName,},
            async (args: any, props: any, data: BufferSource|undefined) => {
                const start = performance.now();
                await this.analyzeMessage(JSON.parse(new TextDecoder().decode(data)));
                await this.rabbitMQChannel.ack({ deliveryTag: args.deliveryTag });
                this.logger.log("Time taken to consume: " + (performance.now() - start) + " ms");
            }
        );
    }

    public async analyzeMessage(data: DeviceData) {
        await Promise.all([
            this.appendToDatabase(data),
            this.verifyAnomaly(data)
        ])
    }

    private async verifyAnomaly(data: DeviceData) {
        if (await this.checkIfAlreadyAnomalyExists(data)) {
            return;
        }

        const { rows } = await this.cockraochDBClient.queryArray<[BigInt]>(`SELECT pulse FROM datareceived WHERE deviceId = '${data.deviceId}' order by datetimeinserted desc limit 10`,);
        const average = this.calculateAverage(rows);
        if (data.pulse > average * 1.5) {
            this.logger.log("Anomaly detected on device " + data.deviceId);
            await this.cockraochDBClient.queryArray(`INSERT INTO alerts (deviceId, avergePulse, currentPulse) VALUES ('${data.deviceId}', ${average}, ${data.pulse})`);
        }
    
    }

    private async checkIfAlreadyAnomalyExists(data: DeviceData) {
        const {rows} = await this.cockraochDBClient.queryArray<[bigint, Date]>(`SELECT deviceid, datetimestarted FROM alerts WHERE deviceid = '${data.deviceId}' order by datetimestarted desc limit 1`,);
        // check if Date is less than 10 minutes ago
        if (rows.length > 0) {
            const date = new Date(rows[0][1]);
            const now = new Date();
            const diff = now.getTime() - date.getTime();
            if (diff < 600000) {
                return true;
            }
        }
        return false;
    }

    private calculateAverage(array: [BigInt][]) {
        let sum = 0;
        for (let i = 0; i < array.length; i++) {
            sum += Number(array[i][0]);
        }
        return sum / array.length;
    }

    private async appendToDatabase(data: DeviceData) {
        await this.cockraochDBClient.queryArray(`INSERT INTO datareceived (deviceId, pulse) VALUES ('${data.deviceId}', ${data.pulse})`);
    }

}