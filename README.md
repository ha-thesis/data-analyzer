### Env variables

`RABBITMQ_USERNAME=data-ingress-api-sa`
`RABBITMQ_PASSWORD=passwd`
`RABBITMQ_HOST=domain.tld/vhost`

If you are using VSCode's launch feature, just create `.env` file in the root of the project and add the above variables.